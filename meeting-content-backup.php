<div class="box-center">
<div class="image row">
<div class="inner-image col-md-6 col-sm-6"><span class="image-inner"><img src="https://www.kesambihijauhotel.com/wp-content/uploads/2017/12/kesambi.jpg" /></span></div>
<div class="inner-image col-md-6 col-sm-6"><span class="image-inner"><img src="https://www.kesambihijauhotel.com/wp-content/uploads/2017/12/kesambi2.jpg" /></span></div>
</div>
<div class="content">

<table class="table">
      <thead>
        <tr>
          <th>Layout</th>
          <th>Capacity</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Block Style</td>
          <td>20</td>
        </tr>
<tr>
          <td>U-Shape</td>
          <td>10</td>
        </tr>
<tr>
          <td>Round Table</td>
          <td>10</td>
        </tr>
<tr>
          <td>Theatre</td>
          <td>25</td>
        </tr>
      </tbody>
    </table>
</div>
</div>
