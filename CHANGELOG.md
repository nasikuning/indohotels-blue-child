## 10 Aug 2018 - v1.0.5 ##
* Adjust meta in header

## 18 July 2018 - v1.0.4 ##
* Tidy up booking form

## 17 July 2018 - v1.0.3 ##
* Tidy up contact form

## 07 July 2018 - v1.0.2 ##
* Tidy up style sitewide

## 11 April 2018 - v1.0.1 ##
* Fix child theme source

## 1 November 2017 - v1.0.0 ##
* Initial Release
